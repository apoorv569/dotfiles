#
# ~/.bashrc
#

bind TAB:menu-complete

#========== History settings ==========#
HISTTIMEFORMAT="%F %T "
HISTCONTROL=ignoredups
HISTSIZE=10000
HISTFILESIZE=10000
shopt -s histappend

#========== Enable VI mode ==========#
set -o vi
bind -m vi-command 'Control-l: clear-screen'
bind -m vi-insert 'Control-l: clear-screen'

#========== PATH ==========#
# PATH="$HOME/.local/bin${PATH:+:${PATH}}"         # adding ~/.local/bin to $PATH
# PATH="$HOME/.local/bin/scripts${PATH:+:${PATH}}" # adding ~/.local/bin/scripts to $PATH
# PATH="$HOME/.local/lib/${PATH:+:${PATH}}"        # adding ~/.local/lib to $PATH
# PATH="$HOME/.config/emacs/bin${PATH:+:${PATH}}"  # adding ~/.config/emacs/bin to $PATH
# PATH="$HOME/Applications${PATH:+:${PATH}}"       # adding ~/Applications to $PATH
# PATH="${CARGO_HOME}:${CARGO_HOME}/bin:${RUSTUP_HOME}:${RUSTUP_HOME}/toolchains/stable-x86_64-unknown-linux-gnu/bin:${RUSTUP_HOME}/toolchains/nightly-x86_64-unknown-linux-gnu/bin:$PATH"
#
#========== Setting defaults ==========#
# export EDITOR="nvim"
# export READER="zathura"
# export VISUAL="nvim"
# export TERMINAL="st"
# export BROWSER="firefox"
# export VIDEO="mpv"
# export IMAGE="sxiv"
# export COLORTERM="truecolor"
# export OPENER="xdg-open"
# export PAGER="less"
# export WM="dwm"


#========== settings some environment variables ==========#
# export HISTFILE="${XDG_STATE_HOME}"/bash/history
# export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#========== Custom aliases ==========#
alias ls='ls --color=auto'
PS1='[\u@\h \W]\$ '

if [ -e "$HOME/.profile" ] && [ -e "/run/current-system/profile/etc/profile.d/nix.sh" ] && "$HOME"/.local/bin/scripts/guix_sys_check; then
  alias config='~/.guix-home/profile/bin/git --git-dir=/home/apoorv/repos/dotfiles --work-tree=/home/apoorv'
else
  alias config='/usr/bin/git --git-dir=/home/apoorv/repos/dotfiles --work-tree=/home/apoorv'
fi

#========== Custom functions ==========#
function find_largest_files() {
    du -h -x -s -- * | sort -r -h | head -20;
}

# Display the current Git branch in the Bash prompt.
#function git_branch() {
#    if [ -d .git ] ; then
#        printf "%s" "($(git branch 2> /dev/null | awk '/\*/{print $2}'))";
#    fi
#}

# Set the prompt.
#function bash_prompt(){
#    PS1='${debian_chroot:+($debian_chroot)}'${blu}'$(git_branch)'${pur}' \W'${grn}' \$ '${clr}
#}

function git_init() {
    if [ -z "$1" ]; then
        printf "%s\n" "Please provide a directory name.";
    else
        mkdir "$1";
        builtin cd "$1";
        pwd;
        git init;
        #touch README.org .gitignore LICENSE;
        #echo "# $(basename $PWD)" >> readme.md
    fi
}

#========== Custom fetch ==========#
#clear

#printf "\n"
#printf "   %s\n" "IP ADDR: $(curl --silent --fail ifconfig.me)"
#printf "   %s\n" "USER: $(echo $USER)"
#printf "   %s\n" "DATE: $(date)"
#printf "   %s\n" "UPTIME: $(uptime -p)"
#printf "   %s\n" "HOSTNAME: $(hostnamectl hostname)"
#printf "   %s\n" "CPU: $(lscpu | grep "Model name" | awk '{for (i=3; i<=NF; i++) printf $i " "; print $NF}')"
#printf "   %s\n" "KERNEL: $(uname -rms)"
#printf "   %s\n" "PACKAGES: $(pacman -Q | wc -l)"
#printf "   %s\n" "RESOLUTION: $(xrandr | awk '/\*/{printf $1" "}')"
#printf "   %s\n" "MEMORY: $(free -m -h | awk '/Mem/{print $3"/"$2}')"
#printf "\n"

#========== Starship prompt ==========#
eval "$(starship init bash)"

# Color definitions.
# You may want to put all your additions into a separate file like
# ~/.bash_colors, instead of adding them here directly.
#if [ -r $HOME/.bash_colors ]; then
#	source $HOME/.bash_colors
#fi

# Bash prompt.
# You may want to put all your additions into a separate file like
# ~/.bash_colors, instead of adding them here directly.
#if [ -r $HOME/.bash_prompt ]; then
#	source $HOME/.bash_prompt
#fi

cdown ()
{
    N=$1
    while [[ $((--N)) > 0 ]]
    do
        clear && echo $N | figlet -c | lolcat && tput civis && sleep 1
    done
}

# Automatically added by the Guix install script.
if [ -n "$GUIX_ENVIRONMENT" ]; then
    if [[ $PS1 =~ (.*)"\\$" ]]; then
        PS1="${BASH_REMATCH[1]} [env]\\\$ "
    fi
fi
