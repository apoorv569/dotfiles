#!/usr/bin/env bash

# Turn off the system bell
xset -b

# Enable screen locking on suspend
xss-lock -- slock &

# Restore the wallpaper
#hsetroot -fill ~/Downloads/index4.jpg &
nitrogen --restore &

picom_cmd="picom --config $HOME/.config/picom/picom.conf"

if ! [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    picom_cmd="${picom_cmd} --experimental-backends"
fi

if [ -z "$(pgrep picom)" ]; then
    ${picom_cmd} &
else
    pkill -9 picom && ${picom_cmd} &
fi

if [ -z "$(pgrep dwmblocks)" ]; then
    dwmblocks >> ~/.dwm/dwmblocks_logs.txt &
else
    pkill -9 dwmblocks && dwmblocks >> ~/.dwm/dwmblocks_logs.txt &
fi

if [ -z "$(pgrep dunst)" ]; then
    dunst &
else
    pkill -9 dunst && dunst &
fi

if [ -z "$(pgrep lxpolkit)" ]; then
    lxpolkit &
else
    pkill -9 lxpolkit && lxpolkit &
fi

if [ -z "$(pgrep redshit)" ]; then
    redshift &
else
    pkill -9 redshift && redshift &
fi

if [ -z "$(pgrep nextcloud)" ]; then
    nextcloud --background &
else
    pkill -9 nextcloud && nextcloud --background &
fi

if [ -z "$(pgrep kdeconnect-indicator)" ]; then
    kdeconnect-indicator &
else
    pkill -9 kdeconnect-indicator && kdeconnect-indicator &
fi

if [ -z "$(pgrep steam)" ]; then
    steam &
else
    pkill -9 steam && steam &
fi

if [ -z "$(pgrep mpd)" ]; then
    mpd ~/.config/mpd/mpd.conf --no-daemon --verbose >> ~/.dwm/mpd_logs.txt  &
else
    pkill -9 mpd && mpd ~/.config/mpd/mpd.conf --no-daemon --verbose >> ~/.dwm/mpd_logs.txt &
fi

if [ -z "$(pgrep xsettingsd)" ]; then
    xsettingsd &
else
    pkill -9 xsettingsd && xsettingsd &
fi

touchpad_id="$(xinput | grep -i Touchpad | awk '{print $6}' | cut -d "=" -f 2)"
trackpoint_id="$(xinput | grep -i Trackpoint | awk '{print $6}' | cut -d "=" -f 2)"
touchpad_tapping_id="$(xinput list-props "$touchpad_id" | grep "Tapping Enabled (" | awk '{print $4}' | sed s/\(// | sed s/\)://)"
touchpad_natural_scrolling_id="$(xinput list-props "$touchpad_id" | grep "Natural Scrolling Enabled (" | awk '{print $5}' | sed s/\(// | sed s/\)://)"
trackpoint_natural_scrolling_id="$(xinput list-props "$trackpoint_id" | grep "Natural Scrolling Enabled (" | awk '{print $5}' | sed s/\(// | sed s/\)://)"
trackpoint_accel_speed_id="$(xinput list-props "$trackpoint_id" | grep "Accel Speed (" | awk '{print $4}' | sed s/\(// | sed s/\)://)"

if [ "$(uname -n)" = "MachineY" ]; then
    xinput set-prop "$touchpad_id" "$touchpad_tapping_id" 1
    xinput set-prop "$touchpad_id" "$touchpad_natural_scrolling_id" 1
    xinput set-prop "$trackpoint_id" "$trackpoint_natural_scrolling_id" 1
    xinput set-prop "$trackpoint_id" "$trackpoint_accel_speed_id" -0.5
else
    echo "Not MachineX"
fi

if ! [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    if [ -z "$(pgrep pipewire)" ]; then
        pipewire &
    else
        pkill -9 pipewire && pipewire &
    fi

    if [ -z "$(pgrep pipewire-pulse)" ]; then
        pipewire-pulse &
    else
        pkill -9 pipewire-pulse && pipewire-pulse &
    fi

    if [ -z "$(pgrep wireplumber)" ]; then
        wireplumber &
    else
        pkill -9 wireplumber && wireplumber &
    fi

    if [ -z "$(pgrep syncthing)" ]; then
        syncthing serve --no-browser --home=/home/apoorv/.config/syncthing >> ~/.dwm/syncthing_logs.txt &
    else
        pkill -9 syncthing && syncthing serve --no-browser --home=/home/apoorv/.config/syncthing >> ~/.dwm/syncthing_logs.txt &
    fi
fi
