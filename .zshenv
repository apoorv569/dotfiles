# typeset -U PATH path

# Other XDG paths
export XDG_DATA_HOME=${XDG_DATA_HOME:="$HOME/.local/share"}
export XDG_CACHE_HOME=${XDG_CACHE_HOME:="$HOME/.cache"}
export XDG_CONFIG_HOME=${XDG_CONFIG_HOME:="$HOME/.config"}
export XDG_STATE_HOME="$HOME/.local/state"

# export XDG_DATA_DIRS="/var/lib/flatpak/exports/share:${HOME}/.local/share/flatpak/exports/share:${XDG_DATA_DIRS}"
# export XDG_DATA_DIRS="${HOME}/.local/share/flatpak/exports/share:${XDG_DATA_DIRS}"

# Disable files
export LESSHISTFILE="-"

# Fixing Paths
export ANDROID_HOME="$XDG_DATA_HOME/android"
export DOOMDIR="$XDG_CONFIG_HOME/doom"
export EMACSDIR="$XDG_CONFIG_HOME/emacs"
export XINITRC="$XDG_CONFIG_HOME/X11/xinitrc"
export XSERVERRC="$XDG_CONFIG_HOME/X11/xserverrc"
#export XAUTHORITY="$XDG_RUNTIME_DIR/Xauthority" # this will break some DMs.
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
export USERXSESSION="$XDG_CACHE_HOME/X11/xsession"
export USERXSESSIONRC="$XDG_CACHE_HOME/X11/xsessionrc"
export ALTUSERXSESSION="$XDG_CACHE_HOME/X11/Xsession"
export INPUTRC="$XDG_CONFIG_HOME"/readline/inputrc
export ERRFILE="$XDG_CACHE_HOME/X11/xsession-errors"
export GNUPGHOME="$XDG_DATA_HOME/gnupg"
export _JAVA_OPTIONS=-Djava.util.prefs.userRoot="$XDG_CONFIG_HOME/java"
export GTK2_RC_FILES="$XDG_CONFIG_HOME/gtk-2.0/gtkrc"
export ZDOTDIR="$XDG_CONFIG_HOME/zsh"
export HISTFILE="$XDG_STATE_HOME/zsh/history"
export CARGO_HOME="$XDG_DATA_HOME/cargo"
export RUSTUP_HOME="$XDG_DATA_HOME/rustup"
export GOPATH="$XDG_DATA_HOME/go"
export RIPGREP_CONFIG_PATH="$XDG_CONFIG_HOME/ripgrep/ripgreprc"
export XMONAD_CONFIG_HOME="$XDG_CONFIG_HOME/xmonad"
export XMONAD_DATA_HOME="$XDG_DATA_HOME/xmonad"
export XMONAD_CACHE_HOME="$XDG_CACHE_HOME/xmonad"
export WGETRC="$XDG_CONFIG_HOME/wget/wgetrc"
export PASSWORD_STORE_DIR="$XDG_DATA_HOME/password-store"
export WINEPREFIX="$XDG_DATA_HOME/wineprefixes/default"
export GEM_SPEC_CACHE="$XDG_CACHE_HOME/gem"
export VAGRANT_HOME="$XDG_DATA_HOME/vagrant"
export XCURSOR_PATH=/usr/share/icons:${XDG_DATA_HOME}/icons
export STARSHIP_CONFIG="$XDG_CONFIG_HOME/starship/starship.toml"
export STARSHIP_CACHE="$XDG_CACHE_HOME/starship/cache"

# path=(
#     "$HOME/.local/bin"
#     "$HOME/.local/bin/scripts"
#     "$HOME/.local/lib/"
#     "$HOME/.config/emacs/bin"
#     "$GOPATH/bin"
#     "/opt/flutter/"
#     "/opt/flutter/bin"
#     "$HOME/Applications"
#     "$CARGO_HOME"
#     "$CARGO_HOME/bin"
#     "$RUSTUP_HOME"
#     "$RUSTUP_HOME/toolchains/stable-x86_64-unknown-linux-gnu/bin"
#     "$RUSTUP_HOME/toolchains/nightly-x86_64-unknown-linux-gnu/bin"
#     $PATH
# )
#
# export PATH

export PATH="$HOME/.local/bin:\
$HOME/.local/bin/scripts:\
$HOME/.local/lib/:\
$HOME/.config/emacs/bin:\
$GOPATH/bin:\
/opt/flutter/:\
/opt/flutter/bin:\
$HOME/Applications:\
$CARGO_HOME:\
$CARGO_HOME/bin:\
$RUSTUP_HOME:\
$RUSTUP_HOME/toolchains/stable-x86_64-unknown-linux-gnu/bin:\
$RUSTUP_HOME/toolchains/nightly-x86_64-unknown-linux-gnu/bin:\
$PATH"

# Guix
GUIX_PROFILE="/home/apoorv/.config/guix/current" . "$GUIX_PROFILE/etc/profile"

# Scaling
#export QT_STYLE_OVERRIDE=gtk
#export QT_AUTO_SCREEN_SCALE_FACTOR=0
#export QT_QPA_PLATFORMTHEME=qt5ct
#export QT_SCALE_FACTOR=1
#export QT_SCREEN_SCALE_FACTORS="1;1;1"
#export GDK_SCALE=1
#export GDK_DPI_SCALE=1

# Default Apps
export EDITOR="nvim"
export READER="zathura"
export VISUAL="nvim"
export TERMINAL="st"
export BROWSER="firefox"
export VIDEO="mpv"
export IMAGE="sxiv"
export COLORTERM="truecolor"
export OPENER="xdg-open"
export PAGER="less"
export WM="dwm"

# Wayland support
if [ "$XDG_SESSION_TYPE" = "wayland" ]; then
    export QT_QPA_PLATFORMTHEME=qt5ct
    export QT_QPA_PLATFORM=wayland;xcb
    export QT_WAYLAND_DISABLE_WINDOWDECORATION=1

    export XCURSOR_SIZE=24

    export XDG_CURRENT_DESKTOP=river
    export XDG_SESSION_DESKTOP=river
    export XDG_CURRENT_SESSION_TYPE=wayland

    export MOZ_ENABLE_WAYLAND=1

    export GDK_BACKEND=wayland=x11

    export _JAVA_AWT_WM_NONREPARENTING=1
fi

# Start blinking
#export LESS_TERMCAP_mb=$(tput bold; tput setaf 2) # green
# Start bold
#export LESS_TERMCAP_md=$(tput bold; tput setaf 2) # green
# Start stand out
#export LESS_TERMCAP_so=$(tput bold; tput setaf 3) # yellow
# End standout
#export LESS_TERMCAP_se=$(tput rmso; tput sgr0)
# Start underline
#export LESS_TERMCAP_us=$(tput smul; tput bold; tput setaf 1) # red
# End Underline
#export LESS_TERMCAP_ue=$(tput sgr0)
# End bold, blinking, standout, underline
#export LESS_TERMCAP_me=$(tput sgr0)

if [ -e "$HOME/.profile" ] && [ -e "/run/current-system/profile/etc/profile.d/nix.sh" ] && [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    source "$HOME"/.profile
    source /run/current-system/profile/etc/profile.d/nix.sh
fi

xrdb ~/.Xresources
