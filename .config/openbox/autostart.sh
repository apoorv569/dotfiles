#!/bin/sh

tint2 -c ~/.config/tint2/minima.tint2rc &
nitrogen --restore &

picom_cmd="picom --config $HOME/.config/picom/picom.conf"

if ! [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    picom_cmd="${picom_cmd} --experimental-backends"
fi

if [ -z "$(pgrep picom)" ]; then
    ${picom_cmd} &
else
    pkill -9 picom && ${picom_cmd} &
fi

if [ -z "$(pgrep variety)" ]; then
    variety &
else
    pkill variety && variety &
fi

if [ "$(uname -n)" = "MachineY" ]; then
    if [ -z "$(pgrep cbatticon)" ]; then
        cbatticon &
    else
        pkill cbatticon && cbatticon &
    fi
fi

if [ -z "$(pgrep nm-applet)" ]; then
    nm-applet &
else
    pkill nm-applet && nm-applet &
fi

if [ -z "$(pgrep dunst)" ]; then
    dunst &
else
    pkill dunst && dunst &
fi

if [ -z "$(pgrep lxpolkit)" ]; then
    lxpolkit &
else
    pkill lxpolkit && lxpolkit &
fi

if [ -z "$(pgrep redshit)" ]; then
    redshift-gtk &
else
    pkill redshift-gtk && redshift-gtk &
fi

if [ -z "$(pgrep nextcloud)" ]; then
    nextcloud --background &
else
    pkill nextcloud && nextcloud --background &
fi

if [ -z "$(pgrep kdeconnect-indicator)" ]; then
    kdeconnect-indicator &
else
    pkill kdeconnect-indicator && kdeconnect-indicator &
fi

if [ -z "$(pgrep steam)" ]; then
    steam -silent &
else
    pkill steam && steam -silent &
fi

if [ -z "$(pgrep mpd)" ]; then
   mpd ~/.config/mpd/mpd.conf --no-daemon --verbose > ~/.dwm/mpd_logs.txt  &
else
    pkill -9 mpd && mpd ~/.config/mpd/mpd.conf --no-daemon --verbose > ~/.dwm/mpd_logs.txt &
fi

if [ -z "$(pgrep xsettingsd)" ]; then
    xsettingsd &
else
    pkill -9 xsettingsd && xsettingsd &
fi

if [ -z "$(pgrep volumeicon)" ]; then
    volumeicon &
else
    pkill -9 volumeicon && volumeicon &
fi

if ! [ -e "/run/current-system/profile/bin/guix-daemon" ]; then

    if [ -z "$(pgrep pipewire)" ]; then
        pipewire &
    else
        pkill -9 pipewire && pipewire &
    fi

    if [ -z "$(pgrep pipewire-pulse)" ]; then
        pipewire-pulse &
    else
        pkill -9 pipewire-pulse && pipewire-pulse &
    fi

    if [ -z "$(pgrep wireplumber)" ]; then
        wireplumber &
    else
        pkill -9 wireplumber && wireplumber &
    fi

    touchpad_id="$(xinput | grep -i Touchpad | awk '{print $6}' | cut -d "=" -f 2)"
    trackpoint_id="$(xinput | grep -i Trackpoint | awk '{print $6}' | cut -d "=" -f 2)"
    touchpad_tapping_id="$(xinput list-props "$touchpad_id" | grep "Tapping Enabled (" | awk '{print $4}' | sed s/\(// | sed s/\)://)"
    touchpad_natural_scrolling_id="$(xinput list-props "$touchpad_id" | grep "Natural Scrolling Enabled (" | awk '{print $5}' | sed s/\(// | sed s/\)://)"
    trackpoint_natural_scrolling_id="$(xinput list-props "$trackpoint_id" | grep "Natural Scrolling Enabled (" | awk '{print $5}' | sed s/\(// | sed s/\)://)"
    trackpoint_accel_speed_id="$(xinput list-props "$trackpoint_id" | grep "Accel Speed (" | awk '{print $4}' | sed s/\(// | sed s/\)://)"

    if [ "$(uname -n)" = "MachineY" ]; then
        xinput set-prop "$touchpad_id" "$touchpad_tapping_id" 1
        xinput set-prop "$touchpad_id" "$touchpad_natural_scrolling_id" 1
        xinput set-prop "$trackpoint_id" "$trackpoint_natural_scrolling_id" 1
        xinput set-prop "$trackpoint_id" "$trackpoint_accel_speed_id" -0.5
    else
        echo "Not MachineX"
    fi
fi

