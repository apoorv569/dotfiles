![Screenshot of my desktop](https://gitlab.com/apoorv569/dotfiles/raw/master/.screenshots/desktop_openbox.png)

# Dependencies
+ tint2
+ picom-tryone-git (AUR)
+ clearine-git (AUR)
+ xfce4-power-manager
+ xfce4-settings
+ xfce-polkit-git (AUR)

# My Keybindings

| Keybinding       | Action                   |
| :---             | :---                     |
| `CTRL + ALT + T` | opens terminal           |
| `ALT + F4`       | closes window with focus |
| `CTRL + ALT + DEL` | open clearine          |
