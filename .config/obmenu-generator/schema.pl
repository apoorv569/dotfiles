#!/usr/bin/perl

# obmenu-generator - schema file

=for comment

    item:      add an item inside the menu               {item => ["command", "label", "icon"]},
    cat:       add a category inside the menu             {cat => ["name", "label", "icon"]},
    sep:       horizontal line separator                  {sep => undef}, {sep => "label"},
    pipe:      a pipe menu entry                         {pipe => ["command", "label", "icon"]},
    file:      include the content of an XML file        {file => "/path/to/file.xml"},
    raw:       any XML data supported by Openbox          {raw => q(...)},
    beg:       begin of a category                        {beg => ["name", "icon"]},
    end:       end of a category                          {end => undef},
    obgenmenu: generic menu settings                {obgenmenu => ["label", "icon"]},
    exit:      default "Exit" action                     {exit => ["label", "icon"]},

=cut

# NOTE:
#    * Keys and values are case sensitive. Keep all keys lowercase.
#    * ICON can be a either a direct path to an icon or a valid icon name
#    * Category names are case insensitive. (X-XFCE and x_xfce are equivalent)

require "$ENV{HOME}/.config/obmenu-generator/config.pl";

## Text editor
my $editor = $CONFIG->{editor};

our $SCHEMA = [
    
    {sep => 'Favorites'},

    #          COMMAND                       LABEL                 ICON
    {item => ['pcmanfm',                 'File Manager',    'system-file-manager']},
    {item => ['st',               'Terminal',        'utilities-terminal']},
    {item => ['emacs',                   'Emacs',           'emacs']},
    {item => ['firefox',                 'Firefox',         'firefox']},
    {item => ['ardour6',                 'Ardour',          'ardour6']},
    {item => ['lmms',                    'LMMS',            'lmms']},
    {item => ['zrythm',                  'Zrythm',          'zrythm']},
    #    {item => ['wine /home/apoorv/.wine/drive_c/Program\ Files/Image-Line/FL\ Studio\ 20/FL.exe',  'FL Studio',        'FL64']},
    {item => ['lutris',                  'Lutris',          'lutris']},
    {item => ['steam',                   'Steam',           'steam']},
    {item => ['rocketchat-desktop',      'Rocket.Chat',     'rocketchat-desktop']},
    {item => ['telegram-desktop',        'Telegram',        'telegram-desktop']},
    {item => ['spotify',                 'Spotify',         'spotify']},
    {item => ['flameshot',               'Flameshot',       'flameshot']},
    #    {item => ['xfce4-settings-manager',  'Settings',        'xfce4-settings-manager']},


#    {item => ['gmrun',             'Run command',     'system-run']},

    {sep => 'Categories'},

    {beg => ['All Applications', 'applications']},
    #          NAME            LABEL                ICON
    {cat => ['utility',     'Accessories', 'applications-utilities']},
    {cat => ['development', 'Development', 'applications-development']},
    {cat => ['education',   'Education',   'applications-science']},
    {cat => ['game',        'Games',       'applications-games']},
    {cat => ['graphics',    'Graphics',    'applications-graphics']},
    {cat => ['audiovideo',  'Multimedia',  'applications-multimedia']},
    {cat => ['network',     'Network',     'applications-internet']},
    {cat => ['office',      'Office',      'applications-office']},
    {cat => ['other',       'Other',       'applications-other']},
    {cat => ['settings',    'Settings',    'applications-accessories']},
    {cat => ['system',      'System',      'applications-system']},
    {end => undef},

    #             LABEL          ICON
    #{beg => ['My category',  'cat-icon']},
    #          ... some items ...
    #{end => undef},

    #            COMMAND     LABEL        ICON
    #{pipe => ['obbrowser', 'Disk', 'drive-harddisk']},

    ## Generic advanced settings
    #{sep       => undef},
    #{obgenmenu => ['Openbox Settings', 'applications-engineering']},
    #{sep       => undef},

    ## Custom advanced settings
    {sep => 'Configuration'},
    {beg => ['Config Files', 'applications-engineering']},

      # Configuration files
#      {item => ["$editor ~/.conkyrc",              'Conky RC',    'text-x-generic']},
      {item => ["$editor ~/.config/alacritty", 'Alacritty Config', 'text-x-generic']},
      {item => ["$editor ~/.config/awesome/rc.lua", 'Awesome Config', 'text-x-generic']},
      {item => ["$editor ~/.config/dunst/dunstrc", 'Dunst Config', 'text-x-generic']},
      {item => ["$editor ~/.config/nvim/init.vim", 'Nvim Config', 'text-x-generic']},
 
    {beg => ['Qtile', 'qtile']},
      {item => ["$editor ~/.config/qtile/config.py", 'Qtile Config', 'text-x-generic']},
      {item => ["$editor ~/.config/qtile/autostart.sh", 'Autostart', 'text-x-generic']},
    {end => undef},

      {item => ["$editor ~/.config/tint2/tint2rc", 'Tint2 Panel', 'text-x-generic']},

    {beg => ['Xmobar', 'xmobar']},
      {item => ["$editor ~/.config/xmobar/xmobarrc", 'xmobarrc', 'text-x-generic']},
      {item => ["$editor ~/.config/xmobar/xmobarrc-l", 'xmobarrc-l', 'text-x-generic']},
    {end => undef},

    {beg => ['Xmonad', 'xmonad']},
      {item => ["$editor ~/.xmonad/xmonad.hs", 'Tint2 Panel', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyAutostart.hs", 'MyAutostart', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Mpdules/MyGridMenu.hs", 'MyGridMenu', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyKeybindings.hs", 'MyKeybindings', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyLayouts.hs", 'MyLayouts', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyPrompts.hs", 'MyPrompts', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyScratchpads.hs", 'MyScratchpads', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyTreeMenu.hs", 'MyTreeMenu', 'text-x-generic']},
      {item => ["$editor ~/.xmonad/lib/Modules/MyVariables.hs", 'MyVariables', 'text-x-generic']},
    {end => undef},



    {end => undef},

      # Openbox category
    {beg => ['Openbox', 'openbox']},
      {item => ["$editor ~/.config/openbox/autostart", 'Openbox Autostart',   'text-x-generic']},
      {item => ["$editor ~/.config/openbox/rc.xml",    'Openbox RC',          'text-x-generic']},
      {item => ["$editor ~/.config/openbox/menu.xml",  'Openbox Menu',        'text-x-generic']},
      {item => ['openbox --reconfigure',               'Reconfigure Openbox', 'openbox']},
      
      {sep  => undef},

      # obmenu-generator category
    {beg => ['Obmenu-Generator', 'accessories-text-editor']},
      {item => ["$editor ~/.config/obmenu-generator/schema.pl", 'Menu Schema', 'text-x-generic']},
      {item => ["$editor ~/.config/obmenu-generator/config.pl", 'Menu Config', 'text-x-generic']},

      {sep  => undef},
      {item => ['obmenu-generator -s -c',    'Generate a static menu',             'accessories-text-editor']},
      {item => ['obmenu-generator -s -i -c', 'Generate a static menu with icons',  'accessories-text-editor']},
      {sep  => undef},
      {item => ['obmenu-generator -p',       'Generate a dynamic menu',            'accessories-text-editor']},
      {item => ['obmenu-generator -p -i',    'Generate a dynamic menu with icons', 'accessories-text-editor']},
      {sep  => undef},

      {item => ['obmenu-generator -d', 'Refresh cache', 'view-refresh']},
    {end => undef},
    {end => undef},
       
    {pipe => ['python2 ~/.local/bin/openbox-pipemenu/ob-randr.py', 'Monitor Settings', '']},
    {pipe => ['~/.local/bin/openbox-pipemenu/ob-compositor', 'Picom', '']},
#    {pipe => ['~/.local/bin/openbox-pipemenu/ob3_theme.c', 'Openbox Theme', '']},
  
    {sep => 'Places'},

           #     {pipe => ['~/.local/bin/openbox-pipemenu/dir-menu.py', 'Computer', '']},
        {pipe => ['~/.local/bin/openbox-pipemenu/cbpp-places-pipemenu', 'Home', '']},
        {pipe => ['~/.local/bin/openbox-pipemenu/obrecent.sh', 'Recent', '']},
        {pipe => ['~/.local/bin/openbox-pipemenu/pcmanfm-bookmarks.pl', 'Bookmarks', '']},
           
    {sep => 'System'},

        {item => ['xrefresh',                   'Refresh',           'xrefresh']},
        {pipe => ['~/.local/bin/openbox-pipemenu/ob-sysinfo.pl', 'System Info', '']},
        {pipe => ['~/.local/bin/openbox-pipemenu/storageinfo', 'Storage', '']},        

    {sep => 'Tools'},
      
        {pipe => ['~/.local/bin/openbox-pipemenu/scrot-pipemenu', 'Scrot', '']},

    {sep => 'Session'},

    ## The xscreensaver lock command
    #    {item => ['light-locker-command -l', 'Lock', 'system-lock-screen']},
    {item => ['slock', 'Lock', 'system-lock-screen']},

    ## This option uses the default Openbox's "Exit" action
#    {exit => ['Exit', 'application-exit']},

    ## This uses the 'oblogout' menu
    {item => ['~/.local/bin/dm-scripts/dm-session', 'Exit', 'application-exit']},
]
