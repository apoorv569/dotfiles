#!/usr/bin/env bash

if [ "$XDG_SESSION_TYPE" == "x11" ]; then
    picom --config ~/.config/picom/picom.conf --experimental-backends &
    nitrogen --restore &
    variety &
    redshift-gtk &
elif [ "$XDG_SESSION_TYPE" == "wayland" ]; then
    lock=$(swaylock -f -i ~/Downloads/Wallpapers/milky-way.jpg -s fill --font "Iosevka Aile" --indicator-idle-visible)

    systemctl --user import-environment WAYLAND_DISPLAY XDG_CURRENT_DESKTOP
    dbus-update-activation-environment --systemd WAYLAND_DISPLAY XDG_CURRENT_DESKTOP=qtile
    swayidle -w timeout 900 "$lock" timeout 960 'swaymsg "output * dpms off"' resume 'swaymsg "output * dpms on"' before-sleep "$lock" &
    swaybg --mode fill --output "*" --image ~/Downloads/Wallpapers/milky-way.jpg &
    wlsunset -S 07:00 -s 18:00 -t 4800 &
fi

xset -b
xsettingsd &
dunst &
nextcloud --background &
syncthing serve --no-browser -home=/home/apoorv/.config/syncthing >> ~/.local/state/syncthing_logs.txt &
steam &
lxpolkit &
kdeconnect-indicator &
mpd ~/.config/mpd/mpd.conf --no-daemon --verbose >> ~/.local/state/mpd_logs.txt  &
udiskie &

if ! [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    pipewire &
    pipewire-pulse &
    wireplumber &
fi
