##### IMPORTS #####
import os
import subprocess
import socket
import re

from libqtile.config import Key, Screen, Group, Drag, Click, Match, ScratchPad, DropDown
from libqtile.lazy import lazy
from libqtile import layout, bar, widget, hook

from typing import List  # noqa: F401
from typing import Iterable

##### CONFIG #####
SUPER = "mod4"                                       # Sets SUPER key to SUPER/WINDOWS
ALT = "mod1"                                         # Sets ALT key for secondary actions
TERMINAL = "alacritty"                               # My terminal of choice
CONFIG = "/home/apoorv/.config/qtile/config.py"      # The Qtile config file location
SESSION = os.environ.get("XDG_SESSION_TYPE")

if SESSION == "wayland":
    lock_cmd = "swaylock"
elif SESSION == "x11":
    lock_cmd = "slock"
else:
    lock_cmd = ""

##### KEYBINDINGS #####
keys = [
    ### The essentials
    Key([SUPER], "Return",
        lazy.spawn(TERMINAL),
        desc='Launches Terminal'),
    Key([ALT], "space",
        lazy.spawn("dmenu_run -c -l 10"),
        desc='Dmenu Run Launcher'),
    Key([SUPER], "Tab",
        # lazy.next_layout(),
        lazy.screen.toggle_group(),
        desc='Move to the last visited group'),
    Key([SUPER, "shift"], "c",
        lazy.window.kill(),
        desc='Kill active window'),
    Key([SUPER, ALT, "shift"], "r",
        lazy.restart(),
        desc='Restart Qtile'),
    Key([SUPER, "shift"], "q",
        lazy.shutdown(),
        desc='Shutdown Qtile'),

    ### Window controls
    Key([SUPER], "k",
        lazy.layout.up(),
        desc='Move focus down in current stack pane'),
    Key([SUPER], "j",
        lazy.layout.down(),
        desc='Move focus up in current stack pane'),
    Key([SUPER, "shift"], "k",
        lazy.layout.shuffle_down(),
        desc='Move windows down in current stack'),
    Key([SUPER, "shift"], "j",
        lazy.layout.shuffle_up(),
        desc='Move windows up in current stack'),
    Key([SUPER], "h",
        lazy.layout.shrink(),
        lazy.layout.increase_nmaster(),
        desc='Expand window (MonadTall), increase number in master pane (Tile)'),
    Key([SUPER], "l",
        lazy.layout.grow(),
        lazy.layout.decrease_nmaster(),
        desc='Shrink window (MonadTall), decrease number in master pane (Tile)'),
    Key([SUPER], "n",
        lazy.layout.normalize(),
        desc='normalize window size ratios'),
    Key([SUPER], "m",
        lazy.layout.maximize(),
        desc='toggle window between minimum and maximum sizes'),
    Key([SUPER, "shift"], "f",
        lazy.window.toggle_floating(),
        desc='toggle floating'),
    Key([SUPER, "shift"], "m",
        lazy.window.toggle_fullscreen(),
        desc='toggle fullscreen'),

    ### Stack controls
    Key([SUPER, "shift"], "space",
        lazy.layout.rotate(),
        lazy.layout.flip(),
        desc='Switch which side main pane occupies (XmonadTall)'),
    Key([SUPER], "space",
        lazy.layout.next(),
        desc='Switch window focus to other pane(s) of stack'),
    Key([SUPER, "control"], "Return",
        lazy.layout.toggle_split(),
        desc='Toggle between split and unsplit sides of stack'),

    ### Dmenu scripts launched with ALT + CTRL + KEY
    Key([SUPER, "shift"], "p",
        lazy.spawn("passmenu"),
        desc='Passmenu'),

    Key([SUPER], "x", lazy.spawn(lock_cmd)),

    # Fn Keys
    Key([], "XF86AudioRaiseVolume",
        lazy.spawn("/home/apoorv/.local/bin/scripts/dunst-volume-wp up")),
    Key([], "XF86AudioLowerVolume",
        lazy.spawn("/home/apoorv/.local/bin/scripts/dunst-volume-wp down")),
    Key([], "XF86AudioMute",
        lazy.spawn("/home/apoorv/.local/bin/scripts/dunst-volume-wp mute")),
    Key([], "XF86MonBrightnessUp",
        lazy.spawn("/home/apoorv/.local/bin/scripts/dunst-brightness-ctl up")),
    Key([], "XF86MonBrightnessDown",
        lazy.spawn("/home/apoorv/.local/bin/scripts/dunst-brightness-ctl down")),
    Key([], "XF86AudioPlay",
        lazy.spawn("playerctl -a play")),
    Key([], "XF86AudioPause",
        lazy.spawn("playerctl -a pause")),
    Key([], "XF86AudioStop",
        lazy.spawn("playerctl -a stop")),
    Key([], "XF86AudioNext",
        lazy.spawn("playerctl -a next")),
    Key([], "XF86AudioPrev",
        lazy.spawn("playerctl -a previous")),

    # Variety Wallpaper Change
    Key([ALT], "Right", lazy.spawn("variety -n")),
    Key([ALT], "Left", lazy.spawn("variety -p")),
]

##### WORKSPACES #####
group_names = [
    ("", {
        'layout': 'monadtall',
    }),
    ("", {
        'layout': 'max',
        'matches':[Match(wm_class="firefox")]
    }),
    ("", {
        'layout': 'monadtall',
        'matches':[Match(wm_class="Pcmanfm")]
    }),
    ("", {
        'layout': 'monadtall',
        'matches':[Match(wm_class="TelegramDesktop"),
                   Match(wm_class="Rocket.Chat"),
                   Match(wm_class="Element")]
    }),
    ("", {
        'layout': 'max',
        'matches':[Match(wm_class="kdevelop"),
                   Match(wm_class="VSCodium"),
                   Match(wm_class="Geany"),
                   Match(wm_class="Emacs")]
    }),
    ("", {
        'layout': 'max',
        'matches':[Match(wm_class="Ardour"),
                   Match(wm_class="Zrythm"),
                   Match(title="Session Setup"),
                   Match(title="Audio/MIDI Setup"),
                   Match(wm_class="Spotify")]
    }),
    ("", {
        'layout': 'monadtall',
        'matches':[Match(wm_class="vlc"),
                   Match(wm_class="Transmission-gtk"),
                   Match(wm_class="Nitrogen"),
                   Match(wm_class="Nextcloud")]
    }),
    ("", {
        'layout': 'max',
        'matches':[Match(wm_class="Inkscape"),
                   Match(wm_class="Gimp")]
    }),
    ("", {
        'layout': 'max',
        'matches':[Match(wm_class="Steam"),
                   Match(title="Friends List"),
                   Match(title="Rockstar Games Launch"),
                   Match(title="Uplay"),
                   Match(title="Origin")]
    })
]

groups = [Group(name, matches=kwargs.get('matches')) for name, kwargs in group_names] # type: ignore

for i, (name, kwargs) in enumerate(group_names, 1):
    keys.append(Key([SUPER], str(i), lazy.group[name].toscreen()))        # Switch to another group
    keys.append(Key([SUPER, "shift"], str(i), lazy.window.togroup(name))) # Send current window to another group

##### LAYOUT SETTINGS #####
layout_theme = {
    "border_width": 2,
    "margin": 8,
    "border_focus": "#2C8A8C",
    "border_normal": "#0A1E1F",
    "single_": 0
}

##### LAYOUTS #####
layouts = [
    layout.MonadTall(**layout_theme),
    layout.MonadWide(**layout_theme),
    layout.Max(
        margin = 0,
        border_width = 0
    ),
    layout.Columns(
        num_columns=3,
        insert_position=1,
        border_focus = "#2C8A8C",
        border_normal =  "#0A1E1F"
    ),
    layout.Floating(**layout_theme, float_rules=[
        # Run the utility of `xprop` to see the wm class and name of an X client.
        Match(wm_class='confirm'),
        Match(wm_class='dialog'),
        Match(wm_class='download'),
        Match(wm_class='error'),
        Match(wm_class='file_progress'),
        Match(wm_class='notification'),
        Match(wm_class='splash'),
        Match(wm_class='toolbar'),
        Match(wm_class='confirmreset'),  # gitk
        Match(wm_class='makebranch'),  # gitk
        Match(wm_class='maketag'),  # gitk
        Match(title='branchdialog'),  # gitk
        Match(title='pinentry'),  # GPG key password entry
        Match(wm_class='ssh-askpass'),  # ssh-askpass
        Match(title='Session Setup'),
        Match(title='Audio/MIDI Setup'),
        Match(title='Origin'),
        Match(title='Uplay'),
        Match(title='Rockstar Games Launcher'),
        Match(title='Friends List'),
        Match(wm_class='steam_app'),
        Match(wm_class='Lightdm-settings'),
        Match(wm_class='Nitrogen'),
        Match(wm_class='Pavucontrol'),
        Match(wm_class='Syncthing GTK'),
        Match(wm_class='Variety'),
    ]),
]

##### WIDGET COLORS #####
colors = [
    ["#2e3440", "#2e3440"], # 0
    ["#3b4252", "#3b4252"], # 1
    ["#434c5e", "#434c5e"], # 2
    ["#4c566a", "#4c566a"], # 3
    ["#d8dee9", "#d8dee9"], # 4
    ["#e5e9f0", "#e5e9f0"], # 5   805E73
    ["#eceff4", "#eceff4"], # 6
    ["#8fbcbb", "#8fbcbb"], # 7
    ["#88c0d0", "#88c0d0"], # 8
    ["#81a1c1", "#81a1c1"], # 9
    ["#5e81ac", "#5e81ac"], # 10
    ["#bf616a", "#bf616a"], # 11
    ["#d08770", "#d08770"], # 12
    ["#ebcb8b", "#ebcb8b"], # 13
    ["#aebe8c", "#aebe8c"], # 14
    ["#b48ead", "#b48ead"]  # 15
]

##### PROMPT #####
prompt = "{0}@{1}: ".format(os.environ["USER"], socket.gethostname())

##### WIDGET SETTINGS #####
widget_defaults = {
    "font": 'CaskaydiaCove Nerd Font',
    "fontsize": 12,
    "padding": 2,
    "background": colors[0]
}

extension_defaults = widget_defaults.copy()

##### WIDGETS #####
def init_widgets_list():
    widgets_list = [
        widget.Sep(
            linewidth = 0,
            padding = 2,
            foreground = colors[2],
            background = colors[0]
        ),
        widget.GroupBox(
            font="Fira Nerd Font",
            fontsize = 22,
            margin_y = 5,
            margin_x = 0,
            padding_y = 5,
            padding_x = 5,
            borderwidth = 3,
            active = colors[6],
            inactive = colors[4],
            rounded = False,
            highlight_color = colors[15],
            highlight_method = "line",
            this_current_screen_border = colors[15],
            this_screen_border = colors [15],
            other_current_screen_border = colors[0],
            other_screen_border = colors[0],
            foreground = colors[6],
            background = colors[0],
            hide_unused = True
        ),
        # widget.Prompt(
        #     prompt=prompt,
        #     font="Mononoki Nerd Font Mono",
        #     padding=10,
        #     foreground = colors[3],
        #     background = colors[1]
        # ),
        # widget.Sep(
        #     linewidth = 0,
        #     padding = 10,
        #     foreground = colors[2],
        #     background = colors[0]
        # ),
        widget.WindowName(
            font="CaskaydiaCove Nerd Font Bold",
            fontsize=14,
            show_state = True,
            foreground = colors[14],
            background = colors[0],
            padding = None
        ),
        # widget.TextBox(
        #     text='',
        #     background = colors[0],
        #     foreground = colors[14],
        #     padding=-7,
        #     fontsize=50
        # ),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground = colors[0],
            background = colors[14],
            padding = 0,
            scale=0.7
        ),
        widget.CurrentLayout(
            foreground = colors[2],
            background = colors[14],
            padding = 5
        ),
        widget.TextBox(
            text='',
            background = colors[0],
            foreground = colors[8],
            padding=-6,
            fontsize=40
        ),
        widget.Net(
            interface = "wlan0",
            format = '↓{down} ↑{up}',
            foreground = colors[1],
            background = colors[8],
            padding = 0
        ),
        widget.TextBox(
            text='',
            background = colors[8],
            foreground = colors[11],
            padding=-6,
            fontsize=40
        ),
        widget.TextBox(
            text=" ",
            foreground=colors[1],
            background=colors[11],
            padding = 0,
            fontsize=14
        ),
        widget.ThermalSensor(
            foreground = colors[1],
            background = colors[11],
            foreground_alert = colors[6],
            tag_sensor= "Package id 0",
            metric = True,
            update_interval = 5
        ),
        widget.TextBox(
            text='',
            background = colors[11],
            foreground = colors[13],
            padding=-6,
            fontsize=40
        ),
        widget.TextBox(
            text=" ",
            foreground=colors[1],
            background=colors[13],
            padding = 0,
            fontsize=14
        ),
        widget.CPU(
            foreground=colors[1],
            background=colors[13],
            padding = 0,
            format = '{load_percent}% ',
            update_interval=1.0,
        ),
        # widget.TextBox(
        #     text=" | ",
        #     foreground=colors[2],
        #     background=colors[10],
        #     padding = 0,
        #     fontsize=14
        # ),
        widget.TextBox(
            text=" ",
            foreground=colors[1],
            background=colors[13],
            padding = 0,
            fontsize=16
        ),
        widget.Memory(
            foreground = colors[1],
            background = colors[13],
            padding = 0,
            format = '{MemUsed}M ',
            update_interval = 1.0,
        ),
        widget.TextBox(
            text='',
            background = colors[13],
            foreground = colors[12],
            padding=-6,
            fontsize=40
        ),
        widget.TextBox(
            text=" ",
            foreground=colors[1],
            background=colors[12],
            padding = 0,
            fontsize=12
        ),
        widget.Backlight(
            foreground = colors[2],
            background = colors[12],
            backlight_name = "amdgpu_bl0"
        ),
        widget.TextBox(
            text='',
            background = colors[12],
            foreground = colors[15],
            padding=-6,
            fontsize=40
        ),
        # widget.TextBox(
        #     text=" |",
        #     foreground=colors[2],
        #     background=colors[12],
        #     padding = 0,
        #     fontsize=14
        # ),
        widget.TextBox(
            text="奔 ",
            foreground=colors[1],
            background=colors[15],
            padding = 0,
            fontsize=14
        ),
        widget.Volume(
            foreground = colors[1],
            background = colors[15],
            padding = 0,
            channel = 'Master',
            device = 'default'
        ),
        widget.TextBox(
            text='',
            background = colors[15],
            foreground = colors[14],
            padding=-6,
            fontsize=40
        ),
        widget.Battery(
            format = '{char} {percent:2.0%}',
            battery = 0,
            foreground = colors[2],
            background = colors[14],
            charge_char = " ",
            discharge_char = " ",
            empty_char = " ",
            full_char = " ",
            unknown_char = " ",
            show_short_text = False,
            low_percentage = 15,
            low_foreground = colors[2],
            notify_below = 15,
            update_interval = 5
        ),
        widget.TextBox(
            text='',
            background = colors[14],
            foreground = colors[10],
            padding=-6,
            fontsize=40
        ),
        widget.TextBox(
            text=" ",
            foreground=colors[1],
            background=colors[10],
            padding = 0,
            fontsize=14
        ),
        widget.Clock(
            foreground = colors[1],
            background = colors[10],
            format="%a, %b %d, %I:%M %p"
        ),
        # widget.Sep(
        #     linewidth = 0,
        #     padding = 5,
        #     foreground = colors[0],
        #     background = colors[8]
        # ),
        # widget.TextBox(
        #     text='',
        #     background = colors[8],
        #     foreground = colors[5],
        #     padding=-7,
        #     fontsize=50
        # ),
        # widget.GenPollText(
        #     background = colors[5],
        #     foreground = colors[2],
        #     update_interval=1,
        #     func=lambda: subprocess.check_output(os.path.expanduser("/home/apoorv/.local/bin/statusbar/bluetooth")).decode()
        # ),
        widget.TextBox(
            text='',
            background = colors[10],
            foreground = colors[9],
            padding=-6,
            fontsize=40
        ),
        widget.CheckUpdates(
            background = colors[9],
            foreground = colors[1],
            display_format = '{updates} ﮮ',
            no_update_string = 'ﮮ',
            distro = 'Arch',
            update_intervale = 60
        ),
        widget.CurrentLayoutIcon(
            custom_icon_paths=[os.path.expanduser("~/.config/qtile/icons")],
            foreground = colors[1],
            background = colors[9],
            padding = 0,
            scale=0.7
        ),
        widget.TextBox(
            text='',
            background = colors[9],
            foreground = colors[0],
            padding=-6,
            fontsize=40
        ),
        widget.Systray(
            background=colors[0],
            padding = 5,
        ),
        widget.Sep(
            linewidth = 0,
            padding = 5,
            foreground = colors[0],
            background = colors[0]
        ),
    ]

    return widgets_list

##### BAR #####

def init_widgets_screen():
    widgets_screen = init_widgets_list()
    return widgets_screen

def init_screens() :
    return [Screen(top=bar.Bar(widgets=init_widgets_screen(), opacity=1.00, size=26))]

if __name__ in ["config", "__main__"]:
    screens = init_screens()
    widgets_list = init_widgets_list()
    widget_screen = init_widgets_screen()

# Drag floating layouts.
mouse = [
    Drag([SUPER], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([SUPER], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([SUPER], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

# floating_layout = layout.Floating(**layout_theme, float_rules=[
#     # Run the utility of `xprop` to see the wm class and name of an X client.
#     Match(wm_class='confirm'),
#     Match(wm_class='dialog'),
#     Match(wm_class='download'),
#     Match(wm_class='error'),
#     Match(wm_class='file_progress'),
#     Match(wm_class='notification'),
#     Match(wm_class='splash'),
#     Match(wm_class='toolbar'),
#     Match(wm_class='confirmreset'),  # gitk
#     Match(wm_class='makebranch'),  # gitk
#     Match(wm_class='maketag'),  # gitk
#     Match(title='branchdialog'),  # gitk
#     Match(title='pinentry'),  # GPG key password entry
#     Match(wm_class='ssh-askpass'),  # ssh-askpass
#     Match(title='Origin'),
#     Match(title='Uplay'),
#     Match(title='Rockstar Games Launcher'),
#     Match(title='Friends List'),
#     Match(wm_class='steam_app'),
#     Match(wm_class='Lightdm-settings'),
#     Match(wm_class='Nitrogen'),
#     Match(wm_class='Pamac-Manager'),
#     Match(wm_class='Pavucontrol'),
#     Match(wm_class='Syncthing GTK'),
#     Match(wm_class='Variety'),
#     Match(title='Session Setup'),
#     Match(title='Audio/MIDI Setup'),
# ])

auto_fullscreen = True
focus_on_window_activation = "smart"

##### AUTOSTART #####
@hook.subscribe.startup_once
def autostart():
    home = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.call([home])
