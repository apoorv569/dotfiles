# Lines configured by zsh-newuser-install
HISTFILE=~/.local/share/zsh/history
HISTSIZE=10000
SAVEHIST=10000
# End of lines configured by zsh-newuser-install

# The following lines were added by compinstall
zstyle :compinstall filename '/home/apoorv/.config/zsh/.zshrc'

fpath+=~/.config/zsh/completions/

autoload -Uz compinit
compinit
# End of lines added by compinstall

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Enable colors and change prompt:
autoload -U colors && colors

autoload -U compinit && compinit -u
zstyle ':completion:*' menu select

# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'

zmodload zsh/complist
compinit
_comp_options+=(globdots)		# Include hidden files.

# vi mode
#bindkey -v
#export KEYTIMEOUT=1

# Keybindings
# Enable searching through history
bindkey '^R' history-incremental-pattern-search-backward

# Home End PgUp PgDown Del Ins
bindkey "^[[H" beginning-of-line
bindkey "^[[4~" end-of-line
bindkey "^[[5~" beginning-of-history
bindkey "^[[6~" end-of-history
bindkey "^[[P" delete-char
bindkey "^[[4h" overwrite-mode

# Ctrl+Left Ctrl+Right
bindkey '^[[1;5D' backward-word
bindkey '^[[1;5C' forward-word

# Ctrl+Bkspc
bindkey '^H' backward-kill-word
bindkey '5~' kill-word

# Edit line in vim buffer ctrl-v
#autoload edit-command-line; zle -N edit-command-line
#bindkey '^v' edit-command-line
# Enter vim buffer from normal mode
#autoload -U edit-command-line && zle -N edit-command-line && bindkey -M vicmd "^v" edit-command-line

# Use vim keys in tab complete menu:
#bindkey -M menuselect 'h' vi-backward-char
#bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -M menuselect 'k' vi-up-line-or-history
#bindkey -M menuselect 'l' vi-forward-char
#bindkey -M menuselect 'left' vi-backward-char
#bindkey -M menuselect 'down' vi-down-line-or-history
#bindkey -M menuselect 'up' vi-up-line-or-history
#bindkey -M menuselect 'right' vi-forward-char
# Fix backspace bug when switching modes
#bindkey "^?" backward-delete-char

# Change cursor shape for different vi modes.
#function zle-keymap-select {
#  if [[ ${KEYMAP} == vicmd ]] ||
#     [[ $1 = 'block' ]]; then
#    echo -ne '\e[1 q'
#  elif [[ ${KEYMAP} == main ]] ||
#       [[ ${KEYMAP} == viins ]] ||
#       [[ ${KEYMAP} = '' ]] ||
#       [[ $1 = 'beam' ]]; then
#    echo -ne '\e[5 q'
#  fi
#}
#zle -N zle-keymap-select

# ci", ci', ci`, di", etc
#autoload -U select-quoted
#zle -N select-quoted
#for m in visual viopp; do
#  for c in {a,i}{\',\",\`}; do
#    bindkey -M $m $c select-quoted
#  done
#done

# ci{, ci(, ci<, di{, etc
#autoload -U select-bracketed
#zle -N select-bracketed
#for m in visual viopp; do
#  for c in {a,i}${(s..)^:-'()[]{}<>bB'}; do
#    bindkey -M $m $c select-bracketed
#  done
#done

# zle-line-init() {
#    zle -K viins # initiate `vi insert` as keymap (can be removed if `bindkey -V` has been set elsewhere)
#    echo -ne "\e[5 q"
# }

# zle -N zle-line-init

# echo -ne '\e[5 q' # Use beam shape cursor on startup.
# precmd() { echo -ne '\e[5 q' ;} # Use beam shape cursor for each new prompt.

# Source configs
# for f in ~/.config/shellconfig/*; do source "$f"; done

# Source some z plugins
source $ZDOTDIR/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh
source $ZDOTDIR/plugins/zsh-completions/zsh-completions.plugin.zsh
source $ZDOTDIR/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source $ZDOTDIR/plugins/zsh-history-substring-search/zsh-history-substring-search.zsh

# Source fzf key-bindings and completions
source $ZDOTDIR/plugins/fzf/completion.zsh
source $ZDOTDIR/plugins/fzf/key-bindings.zsh

# Source aliases
source $ZDOTDIR/custom/aliases.zsh
# Source custom functions
source $ZDOTDIR/custom/functions.zsh

# PATH variable
PATH="$HOME/.local/bin:\
$HOME/.local/bin/scripts:\
$HOME/.local/lib/:\
$HOME/.config/emacs/bin:\
$GOPATH/bin:\
/opt/flutter/:\
/opt/flutter/bin:\
$HOME/Applications:\
${CARGO_HOME}:\
${CARGO_HOME}/bin:\
${RUSTUP_HOME}:\
${RUSTUP_HOME}/toolchains/stable-x86_64-unknown-linux-gnu/bin:\
${RUSTUP_HOME}/toolchains/nightly-x86_64-unknown-linux-gnu/bin:\
${PATH}"

# Starship prompt
if [[ "$TERM" == "dumb" ]]
then
    unsetopt zle
    unsetopt prompt_cr
    unsetopt prompt_subst
    unfunction precmd
    unfunction preexec
    PS1='$ '
else
    eval "$(starship init zsh)"
fi

# Fancy stuff to show when terminal is opened for the first time
# neofetch
# figlet -csf3d "HACK THE PLANET" | lolcat
# figlet -cstfBloody "CANNOT ESCAPE FROM THE REAPER\!" | lolcat
fm6000 --color random --random-dir /home/apoorv/.local/bin/scripts/ascii_arts --not-de --margin 8 --gap 10 --length 20

# =============CUSTOM FETCH==============
#clear
#printf "\n"
#printf "   %s\n" "IP ADDR: $(curl --silent --fail ifconfig.me)"
#printf "   %s\n" "USER: $(echo $USER)"
#printf "   %s\n" "DATE: $(date)"
#printf "   %s\n" "UPTIME: $(uptime -p)"
#printf "   %s\n" "HOSTNAME: $(hostnamectl hostname)"
#printf "   %s\n" "CPU: $(lscpu | grep "Model name" | awk '{for (i=3; i<=NF; i++) printf $i " "; print $NF}')"
#printf "   %s\n" "KERNEL: $(uname -rms)"
#printf "   %s\n" "PACKAGES: $(pacman -Q | wc -l)"
#printf "   %s\n" "RESOLUTION: $(xrandr | awk '/\*/{printf $1" "}')"
#printf "   %s\n" "MEMORY: $(free -m -h | awk '/Mem/{print $3"/"$2}')"
#printf "\n"
#==============================================================================

# Use ~~ as the trigger sequence instead of the default **
export FZF_COMPLETION_TRIGGER='~~'

# Options to fzf command
export FZF_COMPLETION_OPTS='--border --info=inline'

# Use fd (https://github.com/sharkdp/fd) instead of the default find
# command for listing path candidates.
# - The first argument to the function ($1) is the base path to start traversal
# - See the source code (completion.{bash,zsh}) for the details.
_fzf_compgen_path() {
  fd --hidden --follow --exclude ".git" . "$1"
}

# Use fd to generate the list for directory completion
_fzf_compgen_dir() {
  fd --type d --hidden --follow --exclude ".git" . "$1"
}

# (EXPERIMENTAL) Advanced customization of fzf options via _fzf_comprun function
# - The first argument to the function is the name of the command.
# - You should make sure to pass the rest of the arguments to fzf.
_fzf_comprun() {
  local command=$1
  shift

  case "$command" in
    cd)           fzf "$@" --preview 'tree -C {} | head -200' ;;
    export|unset) fzf "$@" --preview "eval 'echo \$'{}" ;;
    ssh)          fzf "$@" --preview 'dig {}' ;;
    *)            fzf "$@" ;;
  esac
}

# direnv hook zsh
_direnv_hook() {
  trap -- '' SIGINT;
  eval "$("/gnu/store/sckzrpmjm6zway0gcqan3zh20swgx2px-direnv-2.32.3/bin/direnv" export zsh)";
  trap - SIGINT;
}

typeset -ag precmd_functions;
if [[ -z "${precmd_functions[(r)_direnv_hook]+1}" ]]; then
  precmd_functions=( _direnv_hook ${precmd_functions[@]} )
fi

typeset -ag chpwd_functions;
if [[ -z "${chpwd_functions[(r)_direnv_hook]+1}" ]]; then
  chpwd_functions=( _direnv_hook ${chpwd_functions[@]} )
fi
