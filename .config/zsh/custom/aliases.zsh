# Aliases
if [ -e "$HOME/.profile" ] && [ -e "/run/current-system/profile/etc/profile.d/nix.sh" ] &&  [ -e "/run/current-system/profile/bin/guix-daemon" ]; then
    alias config='~/.guix-home/profile/bin/git --git-dir=/home/apoorv/repos/dotfiles --work-tree=/home/apoorv'
else
    alias config='/usr/bin/git --git-dir=/home/apoorv/repos/dotfiles --work-tree=/home/apoorv'
fi

alias rm="rm -iv"
alias cp="cp -iv"
alias mv="mv -iv"
alias vim=nvim
alias ls="eza --icons"
alias ll="eza -lh --icons"
alias la="eza -lah --icons"
alias ld="eza -lah --group-directories-first --icons"
alias mbsync="mbsync -c "$XDG_CONFIG_HOME"/isync/mbsyncrc"
alias ip="ip -color"
alias radeontop="radeontop --color"
alias gdb="gdb -n -x $XDG_CONFIG_HOME/gdb/init"
alias wget="wget --hsts-file="$XDG_DATA_HOME/wget/wget-history""
alias diff="diff --color=always"
alias grep='grep --color=auto'
#alias ydown='youtube-dl --embed-thumbnail --add-metadata'
#alias ydownall='youtube-dl --download-archive downloaded.txt -iv'
#alias ydownallaudio='youtube-dl --download-archive downloaded.txt -ixv'
#alias ydownaudio='youtube-dl -f bestaudio --extract-audio --embed-thumbnail --add-metadata'
#alias yformat='youtube-dl -F'
#alias yplay='mpv --no-audio-display --term-osd force --term-osd-bar --term-osd-bar-chars [##-] --ytdl --ytdl-format="mp4[height<=?480] --ytdl-raw-options=playlist-start=1"'
#alias yy='mpv --really-quiet --autofit=25% --geometry=-25-40 --ytdl --ytdl-format="mp4[height<=?1080]" --ytdl-raw-options=playlist-start=1'
alias ssha="eval $(ssh-agent) && ssh-add"
alias wgu="sudo wg-quick up wg0"
alias wgd="sudo wg-quick down wg0"
alias pacin="pacman -Slq | fzf -m --preview 'cat <(pacman -Si {1}) <(pacman -Fl {1} | awk \"{print \$2}\")' | xargs -ro sudo pacman -S"
alias paruin="paru -Slq | fzf -m --preview 'cat <(paru -Si {1}) <(paru -Fl {1} | awk \"{print \$2}\")' | xargs -ro  paru -S"
alias pacrem="pacman -Qq | fzf --multi --preview 'pacman -Qi {1}' | xargs -ro sudo pacman -Rns"
alias installed="grep -i installed /var/log/pacman.log"
alias mkgrub='sudo grub-mkconfig -o /boot/grub/grub.cfg'
alias es='find ~/.local/bin/scripts -type f | fzf --height 15 --margin 1,60,1,1 --header "Edit scripts" --header-first | xargs -I{} nvim {}'
alias ec='find ~/.config/ -type f | fzf --height 15 --margin 1,60,1,1 --header "Edit configs" --header-first | xargs -I{} nvim {}'
