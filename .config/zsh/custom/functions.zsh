# Pipewire helper functions to control buffer size and sample rate
pwbs ()
{
  bs=$1
  pw-metadata -n settings 0 clock.force-quantum "$bs"
}

pwsr ()
{
  sr=$1
  pw-metadata -n settings 0 clock.force-rate "$sr"
}

# Create new AUR package
new_aur_pkg() {
    git -c core.sshCommand='ssh -i ~/.ssh/AUR' clone ssh://aur@aur.archlinux.org/"$1".git ~/repos/AUR/"$1"
    cd ~/repos/AUR/"$1"/
    cp /usr/share/pacman/PKGBUILD.proto ./PKGBUILD
    exa -lah --icons
}
