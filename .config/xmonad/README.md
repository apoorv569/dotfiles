![Screenshot of my desktop](https://gitlab.com/apoorv569/dotfiles/raw/master/.screenshots/desktop_xmonad.png)
Xmonad is a dynamically tiling X11 window manager that is written and
configured in Haskell. Official documentation: [[https://xmonad.org][https://xmonad.org]]

The config is broken into Modules that can be found in the lib/Modules.

# Dependencies
+ picom-tryone-git (AUR)
+ xfce4-power-manager
+ xfce4-settings
+ xfce-polkit-git (AUR)

# My Keybindings

The MODKEY is set to the Super key (aka the Windows key).  I try to keep the
keybindings consistent with all of my window managers.

I am using the Xmonad.Util.EZConfig module which allows keybindings
to be written in simpler format.

| Keybinding | Action |
|:--- | :--- |
| `MODKEY + RETURN` | opens terminal |
| `MODKEY + SHIFT + RETURN` | opens run launcher |
| `MODKEY + TAB` | rotates through the available layouts |
| `MODKEY + SHIFT + c` | closes window with focus |
| `MODKEY + SHIFT + r` | restarts xmonad |
| `MODKEY + SHIFT + q` | quits xmonad |
| `MODKEY + 1-9` | switch focus to workspace (1-9) |
| `MODKEY + SHIFT + 1-9` | send focused window to workspace (1-9) |
| `MODKEY + j` | windows focus down (switches focus between windows in the stack) |
| `MODKEY + k` | windows focus up (switches focus between windows in the stack) |
| `MODKEY + SHIFT + j` | windows swap down (swap windows in the stack) |
| `MODKEY + SHIFT + k` | windows swap up (swap the windows in the stack) |
| `MODKEY + h` | shrink window (decreases window width) |
| `MODKEY + l` | expand window (increases window width) |
