----------------------------------------CUSTOM XMOBAR CONFIG---------------------------------------

Config { 
    
    -- appearance
      font    = "xft:Mononoki Nerd Font:pixelsize=12:antialias=true:hinting=true"
    , additionalFonts = [ "xft:FontAwesome:pixelsize=10" ]
    , bgColor = "#1d1f21"
    , fgColor = "#b5bd68"
    , position = TopSize L 100 24
    , alpha = 255
    
    -- general behavior
    , lowerOnStart = True    -- send to bottom of window stack on start
    , hideOnStart = False    -- start with window unmapped (hidden)
    , allDesktops = True     -- show on all desktops
    , persistent = True      -- enable/disable hiding (True = disabled)
    , iconRoot = "/home/apoorv/.xmonad/xpm/"  -- default: "."

    -- layout
    , sepChar = "%"          -- delineator between plugin names and straight text
    , alignSep = "}{"        -- separator between left-right alignment
    , template = " <action=`xdotool key super+shift+t`><icon=haskell_20.xpm/></action> \
                \<fc=#373b41>|</fc> %UnsafeStdinReader% }{\
                \<fc=#373b41>|</fc> <action=`alacritty -e curl https://wttr.in` button=1>%VIDP%</action> \
                \<fc=#373b41>|</fc> %dynnetwork% \
                \<fc=#373b41>|</fc> <action=`alacritty -e sensors` button=1>%thermal3%</action> \
                \<fc=#373b41>|</fc> <action=`alacritty -e htop` button=1>%cpu%</action><action=`alacritty -e htop` button=1>%memory%</action> \
                \<fc=#373b41>|</fc> %bright%<action=`pavucontrol` button=1><action=`pactl set-sink-mute @DEFAULT_SINK@ toggle` button=2> %default:Master%</action></action> \
                \<fc=#373b41>|</fc> %battery% \
                \<fc=#373b41>|</fc>%webcam%\
                \<fc=#373b41>|</fc>%microphone%\
                \<fc=#373b41>|</fc>%usb%\
                \<fc=#373b41>|</fc>%bluetooth%\
--                \%locks%\
                \<fc=#373b41>|</fc> %internet% \
                \<fc=#373b41>|</fc> %pacupdate% \
                \<fc=#373b41>|</fc> %date% \
                \<fc=#373b41>|</fc>%trayerpad%"
   
    -- plugins
    --   command options:
    --   --Low sets the low cutoff
    --   --High sets the high cutoff
    --
    --   --low sets the color below --Low cutoff
    --   --normal sets the color between --Low and --High cutoffs
    --   --High sets the color above --High cutoff
    --
    --   --template option controls how the plugin is displayed. Text
    
    , commands = 
         -- Time and date
         [ Run Date "<fc=#8abeb7>\xf5ef %a, %b %d, %I:%M %p</fc>" "date" 10
         -- Network
         , Run DynNetwork
                              ["--template"          , "<fc=#81a2be>\xe353 <rx>Kb/s \xe340 <tx>Kb/s</fc>"
                              ] 20
         -- Weather
         , Run Weather "VIDP"
                              [ "--template"         , "<fc=#81a2be>\xfa8f <tempC>°C</fc>"
                              , "-L"                 , "50"
                              , "-H"                 , "80"
                              , "--low"              , "#93a1a1"
                              , "--normal"           , "#93a1a1"
                              , "--high"             , "#93a1a1"
                              ] 36000
         -- Cpu usage in percent
         , Run Cpu 
                              ["--template"          ,"<fc=#f0c674>\xf6c4 <total>%</fc> "
                              ,"-H"                  ,"50"
                              ,"--high"              ,"red"
                              ] 20
         -- Ram used number and percent
         , Run Memory 
                              ["--template"          , "<fc=#f0c674>\xf85a <used>Mb</fc>"
                              ] 20
         -- Thermal Monitor
         , Run ThermalZone 3 
                              ["--template"          ,"<fc=#cc6666>\xf2c7 <temp>°C</fc>"
                              ,"-L"                  ,"40"
                              ,"-H"                  ,"79"
                              ,"-h"                  ,"red"
                              ,"-n"                  ,"yellow"
                              ,"-l"                  ,"green"
                              ] 10
         -- Battery Monitor
         , Run Battery 
                              ["--template"          ,"<acstatus> <fc=#b5bd68><left>%</fc>"
                              ,"--"
                              ,"-L"                  ,"15"
                              ,"-H"                  ,"75"
                              ,"-h"                  ,"green"
                              ,"-m"                  ,"yellow"
                              ,"-l"                  ,"red"
                              ,"-i"                  ,"<fc=#a3be8c>\xf583</fc>"
                              ,"-o"                  ,""
                              ,"-O"                  ,"<fc=#5e81ac>\xf58a</fc>"
                              ,"--highs"             ,"<fc=#a3be8c>\xf581</fc>"
                              ,"--mediums"           ,"<fc=#ebcb8b>\xf57d</fc>"
                              ,"--lows"              ,"<fc=#ebcb8b>\xf582</fc>"
                              ,"--on-icon-pattern"   ,""
                              ,"-A"                  ,"15"
                              ,"-a"                  ,"notify-send -u critical 'Battery running out!!'"
                              ] 10
         -- Brightness
         , Run Brightness 
                              ["--template"          , "<fc=#de935f>\xf5df <percent>%</fc>"
                              , "--"
                              , "-D"                 , "intel_backlight"
                              ] 60
         -- Volume
         , Run Volume "default" "Master" 
                              ["--template"          ,"<status> <fc=#b294bb><volume>%</fc>"
                              , "--"
                              , "-C"                 ,"green"
                              , "-c"                 ,"red"
                              , "-O"                 ,""
                              , "-o"                 ,"<fc=#bf616a>\xfc5d</fc>"
                              , "-H"                 ,"75"
                              , "-L"                 ,"25"
                              , "-h"                 ,"<fc=#fcb3f3>\xf028</fc>"
                              , "-m"                 ,"<fc=#d199ca>\xfa7d</fc>"
                              , "-l"                 ,"<fc=#b294bb>\xfa7f</fc>"
                              ] 10
         -- Locks
         , Run Locks
         -- Pacman updates
         , Run Com "/home/apoorv/.local/bin/statusbar/pacupdate" [] "pacupdate" 36000
         -- Trayer padding
         , Run Com "/home/apoorv/.config/xmobar/trayer-padding-icon.sh" [] "trayerpad" 20
         -- Microphone
         , Run Com "/home/apoorv/.local/bin/statusbar/microphone" [] "microphone" 20
         -- Webcam
         , Run Com "/home/apoorv/.local/bin/statusbar/webcam" [] "webcam" 20
         -- Bluetooth
         , Run Com "/home/apoorv/.local/bin/statusbar/bluetooth" [] "bluetooth" 20
         -- Usb
         , Run Com "/home/apoorv/.local/bin/statusbar/usb" [] "usb" 20
         -- Internet
         , Run Com "/home/apoorv/.local/bin/statusbar/internet" [] "internet" 20
         -- Weather2
         , Run Com "/home/apoorv/.local/bin/statusbar/weather2" [] "weather2" 20
         -- Prints out the left side items such as workspaces, layout, etc.
         , Run UnsafeStdinReader
         ]
      }
