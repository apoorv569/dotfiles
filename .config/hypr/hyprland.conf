input {
    kb_layout = us
    kb_variant =
    kb_model =
    kb_options =
    kb_rules =

    repeat_rate = 65
    repeat_delay = 300

    follow_mouse = 1

    touchpad {
        natural_scroll = true
        disable_while_typing = true
        clickfinger_behavior = false
        middle_button_emulation = false
        tap-to-click = true
        drag_lock = true
    }

    sensitivity = 0
}

# Example per-device config
device {
    name = epic-mouse-v1
    sensitivity = -0.5
}

general {
    gaps_in = 5
    gaps_out = 5

    border_size = 2
    col.active_border = rgba(33ccffee) rgba(00ff99ee) 45deg
    col.inactive_border = rgba(595959aa)

    layout = master
}

decoration {
    rounding = 2
    # multisample_edges = true

    active_opacity = 1.0
    inactive_opacity = 1.0

    drop_shadow = true
    shadow_range = 20
    shadow_render_power = 3
    shadow_ignore_window = true
    shadow_offset = 1 2
    col.shadow = rgba(1a1a1aee)

    blur {
        enabled = true
        size = 8
        passes = 2
        new_optimizations = true
    }

    blurls = lockscreen
}

animations {
    enabled = true

    bezier = myBezier, 0.05, 0.9, 0.1, 1.05
    bezier = slow, 0, 0.85, 0.3, 1
    bezier = overshot, 0.7, 0.6, 0.1, 1.1
    bezier = bounce, 1, 1.6, 0.1, 0.85
    bezier = slingshot, 1, -2, 0.9, 1.25
    bezier = nice, 0, 6.9, 0.5, -4.20
    bezier = smoothOut, 0.36, 0, 0.66, -0.56
    bezier = smoothIn, 0.25, 1, 0.5, 1

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = windowsMove, 1, 4, default
    animation = border, 1, 10, default
    animation = borderangle, 1, 8, default
    animation = fade, 1, 7, default
    animation = fadeDim, 1, 10, smoothIn
    animation = workspaces, 1, 6, default
}

dwindle {
    pseudotile = true
    preserve_split = true
    no_gaps_when_only = true
}

master {
    new_is_master = false
    mfact = 0.50
    orientation = left
    no_gaps_when_only = true
    # always_center_master = true
}

gestures {
    workspace_swipe = true
    workspace_swipe_distance = 400
    workspace_swipe_invert = true
    workspace_swipe_min_speed_to_force = 30
    workspace_swipe_cancel_ratio = 0.5
    workspace_swipe_create_new = false
    workspace_swipe_forever = true
}

binds {
    pass_mouse_when_bound = false
    workspace_back_and_forth = true
}

misc {
    vfr = true
    vrr = true
    disable_hyprland_logo = true
    disable_splash_rendering = true
    enable_swallow = true
    swallow_regex = ^(foot|kitty|Alacritty)$
    no_direct_scanout = true
    focus_on_activate = false
    animate_manual_resizes = true
    animate_mouse_windowdragging = true
    mouse_move_enables_dpms = true
}

# plugin {
#     nstack {
#         layout {
#             orientation=left
#             new_on_top=0
#             new_is_master=0
#             no_gaps_when_only=1
#             special_scale_factor=0.8
#             inherit_fullscreen=1
#             stacks=3
#             center_single_master=0
#             mfact=0.5
#         }
#     }
# }

# Sourcing external config files
source = ~/.config/hypr/configs/env.conf
source = ~/.config/hypr/configs/autostart.conf
source = ~/.config/hypr/configs/keybinds.conf
source = ~/.config/hypr/configs/monitors.conf
source = ~/.config/hypr/configs/window_rules.conf
