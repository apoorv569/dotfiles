#!/usr/bin/env bash

# You can call this script like this:
# $ ./volumeControl.sh up
# $ ./volumeControl.sh down
# $ ./volumeControl.sh mute

# Script modified from these wonderful people:
# https://github.com/dastorm/volume-notification-dunst/blob/master/volume.sh
# https://gist.github.com/sebastiencs/5d7227f388d93374cebdf72e783fbd6a

function get_volume {
 # amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1
 pactl list sinks | grep "Volume:" | awk 'NR==1 {print $5}' | sed 's/[^0-9]*//g'
}

function is_mute {
 # amixer get Master | grep '%' | grep -oE '[^ ]+$' | grep off > /dev/null
 pactl list sinks | grep "Mute:" | awk '{print $2}' | grep yes > /dev/null
}

# For showing the volume outpute after the progress bar.
# ALSA
#vol_get=$(amixer get Master | grep '%' | head -n 1 | cut -d '[' -f 2 | cut -d '%' -f 1)

# Pulseaudio
vol_get=$(pactl list sinks | grep "Volume:" | awk 'NR==1 {print $5}' | sed 's/[^0-9]*//g')
 
if [[ "$(pactl list sinks | grep "Mute:" | awk '{print $2}' | grep yes > /dev/null)" != "" ]]; then
     icon="~/.local/bin/vol-mute.png" 
elif [ "$vol_get" -lt "26" ]; then
	icon="~/.local/bin/vol-off.png"
elif [ "$vol_get" -lt "51" ] && [ "$vol_get" -gt "24" ]; then
	icon="~/.local/bin/vol-low.png"
elif [ "$vol_get" -lt "76" ] && [ "$vol_get" -gt "49" ]; then
	icon="~/.local/bin/vol-med.png"
else
	icon="~/.local/bin/vol-high.png"
fi

function send_notification {
 
  if is_mute ; then
    dunstify -i $iconMuted -r 2593 -u normal "mute"
  else
    volume=$(get_volume)
   
    # Make the bar with the special character ─ (it's not dash -)
    # https://en.wikipedia.org/wiki/Box-drawing_character
    bar=$(seq --separator="─" 0 "$((volume / 5))" | sed 's/[0-9]//g')
    
    # Spaces is used for keeping the output volume to the right side of the notification.
    spaces=$(seq --separator=" " 0 "$(( 30 - (volume / 5) ))" | sed 's/[0-9]//g')

    # Send the notification
#    dunstify -i $icon -r 2593 -u normal "  ${bar}${spaces} $vol_get %"
#    notify-send -i $icon -h string:x-dunst-stack-tag:dunst-vol -h int:value:$volume "Volume"
    dunstify -i $icon -r 2593 -u normal -h int:value:$volume -h string:hlcolor:#727fc2 "Volume "$volume"%"

  fi
}

case $1 in
  up)
   # set the volume on (if it was muted)
   # ALSA 
  # amixer -D pulse set Master on > /dev/null
   
   # Pulseaudio
   pactl set-sink-mute @DEFAULT_SINK@ false > /dev/null
    
   # up the volume (+ 5%)
   # ALSA
  # amixer -D pulse sset Master 5%+ > /dev/null
   
   # Pulseaudio
   pactl set-sink-volume @DEFAULT_SINK@ +2%
    
   send_notification
   ;;
  
  down)
   # ALSA 
  # amixer -D pulse set Master on > /dev/null
   
   # Pulseaudio
   pactl set-sink-mute @DEFAULT_SINK@ false > /dev/null
   
   # ALSA
   # amixer -D pulse sset Master 5%- > /dev/null
   
   # Pulseaudio
   pactl set-sink-volume @DEFAULT_SINK@ -2%
    
   send_notification
   ;;
  
  mute)
   # toggle mute
   
   # ALSA 
  # amixer -D pulse set Master 1+ toggle > /dev/null
   
   # Pulseaudio
   pactl set-sink-mute @DEFAULT_SINK@ toggle > /dev/null
   
   send_notification
   ;;
esac
