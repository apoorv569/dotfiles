#!/usr/bin/env perl
use strict;
use warnings;
use autodie;
use File::Basename;

# dbbolton
# danielbarrettbolton@gmail.com

my $filemanager = "pcmanfm -n";
my $bookmarks_file = "$ENV{HOME}/.config/gtk-3.0/bookmarks";
open(my $in, "<", "$bookmarks_file");

my @lines = <$in>;
chomp(@lines);

# Heading #############################
print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
print "<openbox_pipe_menu>\n";

# Default bookmarks in pcmanfm #########

print "<item label=\"File System\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." / \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<separator />\n";

print "<item label=\"$ENV{USER}\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." $ENV{HOME} \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Bin\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/bin \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Desktop\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Desktop \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Documents\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Documents \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Downloads\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Downloads \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Music\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Music \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Pictures\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Pictures \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Public\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Public \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Scrots\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Pictures/scrots \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Temp\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Temp \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Videos\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/Videos \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<item label=\"Wallpaper\">\n";
print " <action name=\"Execute\">\n";
print "  <execute>\n";
print "   ".$filemanager." /home/$ENV{USER}/wallpaper \n";
print "  </execute>\n";
print " </action>\n";
print "</item>\n";

print "<separator />\n";

# User-specified bookamrks ############
foreach (@lines) {
    print "<item label=\"".(split / /, $_)[1]."\">\n";
    print " <action name=\"Execute\">\n";
    print "  <execute>\n";
    print "   ".$filemanager." ".(split / /, $_)[0]."\n";
    print "  </execute>\n";
    print " </action>\n";
    print "</item>\n";
}

print "</openbox_pipe_menu>\n";

close $in;
