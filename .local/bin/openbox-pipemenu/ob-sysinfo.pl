#!/usr/bin/env perl
use strict;
use warnings;
use autodie;
use POSIX qw(strftime);
use XML::Simple;
use Data::Dumper;

# Distro -------------------------------------------------------------------
open (my $issue, "<", "/etc/issue");
my $distro;
while (<$issue>) {
  if (/^[^\s]/) {
    $distro = (split / /, ((split /\\/)[0]))[0];
    last;
   }
}
close $issue;

# Host ---------------------------------------------------------------------
my $host = `uname -n`;
chomp $host;

# Kernel -------------------------------------------------------------------
my $kernel = `uname -r`;
chomp $kernel;

# CPU -------------------------------------------------------------------
my $cpu = `cat /proc/cpuinfo | grep 'model name' | cut -f 2 -d':' | uniq`;
chomp $cpu;

# Service Manager -------------------------------------------------------------------
my $servman = `cat /proc/1/comm`;
chomp $servman;

# Adapter -------------------------------------------------------------------
my $adapter = `acpi -a | grep "Adapter" | cut -f 2 -d':'`;
chomp $adapter;

# Battery -------------------------------------------------------------------
my $battery = `acpi -b | grep "Battery" | cut -f 2 -d':'`;
chomp $battery;

# Brightness -------------------------------------------------------------------
my $brightness = `cat /sys/class/backlight/nv_backlight/brightness`;
chomp $brightness;

# Load ---------------------------------------------------------------------
my $load = (split ' ', (split ':', `uptime`)[4])[0];
chop $load;

# Machine ------------------------------------------------------------------
my $machine = `uname -m`;
chomp $machine;

# Memory -----------------------------------------------------------------------
my $memory = `free -m | sed -n '2,2p' | tr -s '[:blank:]''' | cut -f 7 -d' '`;
chomp $memory;

# Swap -----------------------------------------------------------------------
my $swap = `free -m | sed -n '3,3p' | tr -s '[:blank:]''' | cut -f 4 -d' '`;
chomp $swap;

# Swappiness --------------------------------------------------------------------
my $swappiness = `cat /proc/sys/vm/swappiness`;
chomp $swappiness;

# Memory (active) ------------------------------------------------------------
open (my $meminfo, "<", "/proc/meminfo");
my $mem_act;
while (<$meminfo>) {
  chomp;
  if (/^Active:/) {
    $mem_act = int(((split)[-2])/1024);
    last;
  }
}
close $meminfo;

# Openbox theme ------------------------------------------------------------
my $file = "$ENV{HOME}/.config/openbox/rc.xml";
my $xs1 = XML::Simple->new();
my $doc = $xs1->XMLin($file);
my $obtheme = $doc->{theme}->{'name'};

# Resolution-------------------------------------------------------------
my $resolution = `xrandr | grep "current" | cut -d ',' -f 2,3`;
chomp $resolution;

# Thermal -------------------------------------------------------------------
my $thermal = `acpi -t | grep "Thermal" | cut -d ' ' -f 3,4,9,10,15,16,21,22,27,28 | cut -d '.' -f 1,3,5,7,9`;
chop $thermal;

# OS -----------------------------------------------------------------------
my $os = `uname -o`;
chomp $os;

# Time ---------------------------------------------------------------------
my $time_date = strftime "%D, %R", localtime;

# Uptime -------------------------------------------------------------------
my $uptime = (split ' ', `uptime`)[0];

# WiFi -------------------------------------------------------------------
my $wifi = `nmcli device status | grep "wifi"`;
chomp $wifi;

# WiFi Inet -------------------------------------------------------------------
my $inet = `ifconfig -a | grep 192 | cut -d ' ' -f 1-14`;
chomp $inet;

# WiFi Performance-------------------------------------------------------------------
my $wifiperf = `nmcli dev wifi | grep "Mbit/s" | tr -s '[:blank:]''' | cut -d ' ' -f 3-9`;
chomp $wifiperf;

# Wired-------------------------------------------------------------------
my $wired = `nmcli device status | grep "ethernet"`;
chomp $wired;


# Writing the pipemenu -----------------------------------------------------

print "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
    . "<openbox_pipe_menu>\n"
    . "<item label=\"+        $ENV{USER}\@$host        +\" />\n"
    . "<separator />"
    . "<item label=\"OS:      $distro $os $machine \" />\n"
    . "<item label=\"Kernel:  $kernel \" />\n"
    . "<item label=\"CPU:     $cpu \" />\n"
    . "<item label=\"Service Manager:  $servman \" />\n"
    . "<separator />"
    . "<item label=\"Adapter:  $adapter \" />\n"
    . "<item label=\"Battery:  $battery \" />\n"
    . "<item label=\"Brightness:  $brightness \" />\n"
    . "<item label=\"Load:    $load \" />\n"
    . "<item label=\"Memory Used:     $mem_act MB\" />\n"
    . "<item label=\"Memory Available:  $memory \" />\n"
    . "<item label=\"Resolution:  $resolution \" />\n"
    . "<item label=\"Swap Free:  $swap \" />\n"
    . "<item label=\"Swappiness Percent:  $swappiness \" />\n"
    . "<item label=\"Theme:   $obtheme \" />\n"
    . "<item label=\"Temp:    $thermal \" />\n"
    . "<item label=\"Uptime:  $uptime \" />\n"
    . "<item label=\"WiFi:  $wifi \" />\n"
    . "<item label=\"# WiFi Inet -------------------------------------------------------------------
my $inet = `ifconfig -a | grep 192 | cut -d ' ' -f 1-11`;
chomp $inet;  $wifi \" />\n"
    . "<item label=\"WiFiperf:  $wifiperf \" />\n"
    . "<item label=\"Wired Net:  $wired \" />\n"
    . "<separator />"
    . "<item label=\"+        $time_date       +\" />\n"
    . "</openbox_pipe_menu>\n";
